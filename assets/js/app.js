// DEFINE APP
var myApp = angular.module('myApp', ['ui.bootstrap']);

// MAIN CONTROLLER
myApp.controller('mainController', ['$scope','$modal', function ($scope, $modal) {
    
    $scope.results = [
    	{
            title: "Promotional Site",
            tags: "bootstrap",
            projectType: "Bootstrap",
            warnText: "Text",
            image: "assets/img/thumbnail-shyanne-ward-com.jpg",
            modalImage: "assets/img/modal-preview-shyannewardcom.png",
            description: "This was a project I did to help my wife and her direct sales business.",
            longDesc: "I created this website to help showcase all that my wife has been doing with her direct sales business. On this site, interested individuals can find information about each company, and there is also links for them to sign up to become either a sales person, or customer.",
            tech: ["CSS3","HTML5","Bootstrap"],
            link: "http://www.shyanneward.com",
        },
        {
            title: "Find A Designer",
            tags: "angularjs",
            projectType: "AngularJS",
            image: "assets/img/thumbnail-find-a-designer.jpg",
            modalImage: "assets/img/modal-preview-find-a-designer.jpg",
            description: "This project was created to showcase Angular's Filter feature.",
            longDesc: "I created this website to help showcase all that my wife has been doing with her direct sales business. On this site, interested individuals can find information about each company, and there is also links for them to sign up to become either a sales person, or customer.",
            tech: ["CSS3","HTML5", "Sass", "AngularJS"],
            link: "http://find.digitalgraphx.net",
        },
     	{
            title: "Digital Graphx Logo",
            tags: "illustrator",
            projectType: "Illustrator",
            description: "This was a logo I made...",
            image: "assets/img/thumbnail-digital-graphx-logo.jpg",
            modalImage: "assets/img/modal-preview-digital-graphx-logo.jpg",
            link: "http://www.behance.net/gallery/40167357/Digital-Graphx-Logo",
            tech: ["Illustrator"]
        },
        {
            title: "Pink Papaya Marketing",
            tags: "illustrator",
            projectType: "Illustrator",
            description: "This was a project I did to help my wife and her direct sales business",
            image: "assets/img/thumbnail-pink-papaya-marketing.jpg",
            modalImage: "assets/img/modal-preview-pink-papaya-marketing.jpg",
            tech: ["Illustrator"]
        },
        {
            title: "IBM Watson Wallpaper",
            tags: "photoshop",
            projectType: "Photoshop",
            description: "I did this project because I wanted a pixel perfect wallpaper for my Lenovo 21.5\" monitor.",
            image: "assets/img/thumbnail-watson-wallpaper.jpg",
            modalImage: "assets/img/modal-preview-watson-wallpaper.jpg",
            tech: ["Photoshop"]
        },
        {
            title: "HTML5 Examples",
            tags: "html5",
            projectType: "HTML5",
            image: "assets/img/thumbnail-html5.jpg",
            description: "This Project was create to help individuals find a local sales consultant",
            tech: ["CSS3","HTML5"]
        },
        {
            title: "CSS3 Examples",
            tags: "css3",
            projectType: "CSS3",
            image: "assets/img/thumbnail-html5.jpg",
            description: "This Project was create to help individuals find a local sales consultant",
            tech: ["CSS3","HTML5"]
        },
        {
            title: "Cucumber Testing",
            tags: "cucumber",
            projectType: "Cucumber",
            image: "assets/img/thumbnail-html5.jpg",
            description: "This Project was create to help individuals find a local sales consultant",
            tech: ["CSS3","HTML5","Cucumber"]
        },
        {
            title: "Drupal Examples",
            tags: "drupal",
            projectType: "Drupal",
            image: "assets/img/thumbnail-html5.jpg",
            description: "This Project was create to help individuals find a local sales consultant",
            tech: ["CSS3","HTML5","Drupal","Drush"]
        },
        {
            title: "Palindrome Checker",
            tags: "javascript",
            projectType: "JavaScript",
            image: "assets/img/thumbnail-palindrome.jpg",
            description: "This project was inspired by my tech interview with IBM Digital.",
            link: "http://palindrome.digitalgraphx.net",
            tech: ["CSS3","HTML5","JavaScript"]
        },
        {
            title: "Python Examples",
            tags: "python",
            projectType: "Python",
            image: "assets/img/thumbnail-html5.jpg",
            description: "This Project was create to help individuals find a local sales consultant",
            tech: ["CSS3","HTML5","Python"]
        },
        {
            title: "Sass Examples",
            tags: "sass",
            projectType: "Sass",
            image: "assets/img/thumbnail-html5.jpg",
            description: "This Project was create to help individuals find a local sales consultant",
            tech: ["CSS3","HTML5","Sass"]
        },
    ];
    
    // SIDEBAR FILTERING
    $scope.tagList = [];
    $scope.sortTag = function(tagValue) {
        var i = $.inArray(tagValue, $scope.tagList);
        if (i > -1) {
            $scope.tagList.splice(i, 1);
        } else {
            $scope.tagList.push(tagValue);
        }
    };
    $scope.tagFilter = function(tags) {
        console.log(tags);
        if ($scope.tagList.length > 0) {
            if ($.inArray(tags.tags, $scope.tagList) < 0)
                return;
        }     
        return tags;
    };
    
    // MODAL
    $scope.openProject = function(selectedItem) {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal.html',
            controller: 'overlayModalCtrl',
            size: 'lg',
            resolve: {
                selectedItem: function() {
                    return selectedItem;
                }
            }
        });
    };
    // FEEDBACK DATA
    $scope.feedback = [
    	{
    		quoteText: '"Based on my experience working with Cody, I would recommend him for a position in testing or evaluating user interfaces."',
    		quoteSource: "- Fred Wu, Ph.D.",
    		sourceRoll:	"Project Manager, IBM Watson Research",
    		active: "active"
    	},
    	{
    		quoteText: '"Cody is a great combination of technical skills and social/leadership skills."',
    		quoteSource: "- Don Tuski, Ph.D.",
    		sourceRoll:	"President, Olivet College (Former)",
    	},
    	{
    		quoteText: '"Cody asked great questions and helped me with the look I was going for. And so speedy too!"',
    		quoteSource: "- Jill Antczak‎",
    		sourceRoll:	"Pink Papaya Premier Team Leader"
    	},
    	{
    		quoteText: '"Cody demonstrated his leadership qualities as president of the Olivet College Computer Science Club."',
    		quoteSource: "- Michael Fredericks",
    		sourceRoll:	"Professor, Olivet College"
    	},
    	{
    		quoteText: '"If anyone needs any type of graphics made Cody Ward is your man! Super fast. Professional and amazing quality!"',
    		quoteSource: "- Alesha Holmden",
    		sourceRoll:	"Le-Vel Brand Promoter"
    	},
    	{
    		quoteText: '"He does amazing work and I\'m so thankful for his patience and resolutions... I highly recommend him!"',
    		quoteSource: "- Heather Mills",
    		sourceRoll:	"Le-Vel Brand Promoter"
    	},
    ];
    
    // EMPLOYMENT DATA
    $scope.employment = [
    	{
    		employer: "IBM, Inc.",
    		image: "assets/img/aside-ibm.png",
    		position: "Front End Developer",
    		duration: "Jul 2013 - Current",
    		link: "https://www.ibm.com"
    	},
    	{
    		employer: "Olivet College",
    		image: "assets/img/aside-olivet.png",
    		position: "Manager, Design",
    		duration: "Nov 2008 - May 2012",
    		link: "https://www.olivetcollege.edu"
    	},
    	{
    		employer: "Best Buy (416)",
    		image: "assets/img/aside-bby.png",
    		position: "Customer Service",
    		duration: "Apr 2011 - Oct 2011",
    		link: "https://www.bestbuy.com"
    	},
    	{
    		employer: "Best Buy (803)",
    		image: "assets/img/aside-bby.png",
    		position: "Mobile Sales (Seasonal)",
    		duration: "Oct 2014 - Feb 2015",
    		link: "https://www.bestbuy.com"
    	},
    ];
    
    // EDUCATION DATA
    $scope.education = [
    	// {
    		// school: "University of Michigan",
    		// image: "assets/img/aside-um.png",
    		// degree: "Master of Science",
    		// field: "Computer Science (Online)",
    		// duration: "Fall 2014 - Current",
    		// link: "https://www.umflint.edu"
    	// },
    	{
    		school: "Olivet College",
    		image: "assets/img/aside-oc.png",
    		degree: "Bachelor of Arts",
    		field: "Computer Science",
    		duration: "Fall 2008 - Spring 2012",
    		link: "https://www.olivetcollege.edu"
    	},
    	{
    		school: "Maple Valley",
    		image: "assets/img/aside-mv.png",
    		degree: "High School Diploma",
    		field: "General Studies",
    		duration: "Sep 2003 - Jun 2007",
    		link: "https://www.mvs.k12.mi.us"
    	}
    ];
}]);

// FOOTER CONTROLLER
myApp.controller('footerController', ['$scope', function ($scope) {
    $scope.social = [
    	{link: "https://www.twitter.com/wardcx90", icon: "fa-twitter"},
    	{link: "https://www.linkedin.com/in/wardcx90", icon: "fa-linkedin"},
    	{link: "https://plus.google.com/u/0/118293986281891953780", icon: "fa-google-plus"},
    	{link: "https://codepen.io/WardCX90", icon: "fa-codepen"},
    	{link: "https://gitlab.com/u/WardCX90", icon: "fa-gitlab"},
    	{link: "https://stackoverflow.com/users/6267090/wardcx90", icon: "fa-stack-overflow"},
    ];
}]);

// CAROUSEL INTERVAL
$('.carousel').carousel({
  interval: 1000 * 10
});

// MODAL CONTROLLER
myApp.controller('overlayModalCtrl', ['$scope','$modalInstance','selectedItem', 
    function ($scope,$modalInstance,selectedItem) {
    $scope.projectTitle = selectedItem.title;
    $scope.imgSrc = selectedItem.image;
    $scope.modalImgSrc = selectedItem.modalImage;
    $scope.longDesc = selectedItem.longDesc;
    $scope.projectType = selectedItem.projectType;
    $scope.projectDesc = selectedItem.description;
    $scope.projectTags = selectedItem.tags;
    $scope.link = selectedItem.link;
    $scope.disclaimer = selectedItem.disclaimer;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);